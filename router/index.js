const express = require("express");
const router = express.Router();
const bodyParse = require("body-parser");


router.get('/', (req, res) => {
    const resultados = {
        numBoleto:req.query.numBoleto,
        nombreCliente:req.query.nombreCliente,
        anosCumplidos:req.query.anosCumplidos,
        destino:req.query.destino,
        tipoViaje:req.query.tipoViaje,
        precio:req.query.precio,
        precioBoleto:req.query.precioBoleto,
        impuesto:req.query.impuesto,
        descuento:req.query.descuento,
        totalAPagar:req.query.totalAPagar
    }
    
    res.render('index.html', resultados);
  });


router.post('/', (req, res)=>{
    const resultados = {
        
        numBoleto:req.body.numBoleto,
        nombreCliente:req.body.nombreCliente,
        anosCumplidos:req.body.anosCumplidos,
        destino:req.body.destino,
        tipoViaje:req.body.tipoViaje,
        precioBoleto:req.body.precioBoleto,
        precio:req.body.precio,
        impuesto:req.body.impuesto,
        descuento:req.body.descuento,
        totalAPagar:req.body.totalAPagar
    }
    res.render('index.html', resultados);
})
  


module.exports = router;